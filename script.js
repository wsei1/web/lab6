let animateMarker;

function initMap() {
	var location = { lat: 51.359, lng: 21.582 };
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 6,
		center: { lat: 51.359, lng: 21.582 },
		center: location,
	});
	new google.maps.Marker({
		position: { lat: 51.359, lng: 21.582 },
		map,
		label: 'Mateusz Domagała',
	});
	new google.maps.Circle({
		strokeColor: '#FF0000',
		strokeOpacity: 0.8,
		strokeWeight: 2,
		fillColor: '#FF0000',
		fillOpacity: 0.35,
		map,
		center: { lat: 51.9194, lng: 19.1451 },
		radius: 350000,
	});
	animateMarker = new google.maps.Marker({
		map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: { lat: 52.2297, lng: 21.0122 },
	});
	animateMarker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (animateMarker.getAnimation() !== null) {
		animateMarker.setAnimation(null);
	} else {
		animateMarker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

window.initMap = initMap;
